@section ("css")
<style>
    .product-details #btn_product{
        background: #CD8835!important;
    }
    .product-details #btn_product:hover{
        color: #CD8835!important;
        background: transparent!important;
    }
</style>
@endsection
      
<form action="{{ route('cart.store') }}" class="" method="post">
        {{ csrf_field() }}

<section class="ftco-section bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 mb-5 ftco-animate">
                <figure class="text-center hide-on-small product-cover-wrap col-md-8">
                    @if(isset($product->cover))
                        <img id="main-image" class="product-cover img-responsive"
                                src="{{ asset("storage/$product->cover") }}?w=400"
                                data-zoom="{{ asset("storage/$product->cover") }}?w=1200">
                    @else
                        <img id="main-image" class="product-cover" src="https://placehold.it/300x300"
                                data-zoom="{{ asset("storage/$product->cover") }}?w=1200" alt="{{ $product->name }}">
                    @endif
                </figure>
                <ul id="thumbnails" style="margin: 0!important;"class="row product-slider owl-carousel ftco-animate list-unstyled">
                    <li>
                        <a href="javascript: void(0)">
                            @if(isset($product->cover))
                            <img class="img-responsive img-thumbnail"
                                    src="{{ asset("storage/$product->cover") }}"
                                    alt="{{ $product->name }}" />
                            @else
                            <img class="img-responsive img-thumbnail"
                                    src="{{ asset("https://placehold.it/180x180") }}"
                                    alt="{{ $product->name }}" />
                            @endif
                        </a>
                    </li>
                    @if(isset($images) && !$images->isEmpty())
                        @foreach($images as $image)
                        <li>
                            <a href="javascript: void(0)">
                            <img class="img-responsive img-thumbnail"
                                    src="{{ asset("storage/$image->src") }}"
                                    alt="{{ $product->name }}" />
                            </a>
                        </li>
                        @endforeach
                    @endif
                </ul>
                {{-- <a href="{{ asset("storage/$product->cover") }}" class="image-popup"><img class="img-responsive img-fluid" src="{{ asset("storage/$product->cover") }}" alt="{{ $product->name }}" /></a> --}}
                {{-- <a href="{{asset("sto/images/image_2.jpg")}}" class="image-popup"><img src="{{asset("front/images/product-1.jpg")}}" class="img-fluid" alt=""></a> --}}
            </div>
            <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                @include('layouts.errors-and-messages')

                <h3>{{ucfirst($product->name)}}</h3>
                <p class="price"><span>{{ config('cart.currency') }} {{$product->price}}</span></p>
                <?php
                    echo $product->description;
                ?>
                <div class="row mt-4">
                        
                    {{-- 
                    @if(isset($productAttributes) && !$productAttributes->isEmpty())
                        <div class="form-group">
                            <label for="productAttribute">Choose Combination</label> <br />
                            <select name="productAttribute" id="productAttribute" class="form-control select2">
                                @foreach($productAttributes as $productAttribute)
                                    <option value="{{ $productAttribute->id }}">
                                        @foreach($productAttribute->attributesValues as $value)
                                            {{ $value->attribute->name }} : {{ ucwords($value->value) }}
                                        @endforeach
                                        @if(!is_null($productAttribute->price))
                                            ( {{ $productAttribute->price }} {{ config('cart.currency_symbol') }})
                                        @endif
                                    </option>
                                @endforeach
                            </select>
                        </div><hr>
                    @endif 
                    --}}
                   
                    <div class="col-md-6 d-flex">
                        <?php
                            $attributs = array();
                        ?>
                        @foreach($productAttributes as $productAttribute)
                            @foreach($productAttribute->attributesValues as $value)
                                <?php
                                    $attributs[$value->attribute->name][] = array("name"=>$value->value, "id"=>$productAttribute->id );
                                ?>  
                            @endforeach
                        @endforeach
                        @foreach($attributs as $key=>$attribut)
                        <?php
                            // var_dump($key);
                            // var_dump($attribut);
                        ?>
                        <div class="attributs mr-3">
                            <label for="">{{$key}}</label>
                            <div class="form-group d-flex">
                                <div class="select-wrap">
                                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                                    <select name="productAttribute" id="productAttribute" class="form-control">
                                        @foreach($attribut as $values)
                                            <option value="{{ $values['id'] }}">
                                                {{ ucwords($values['name']) }}
                                            </option> 
                                        @endforeach
                                    </select>
                                    </div>
                                </div> 
                            {{--
                            --}}
                        </div>   
                        @endforeach
                    </div>
                    <div class="w-100"></div>
                    <div class="input-group col-md-12 d-flex mb-3">
                        <span class="input-group-btn mr-2">
                            <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                                <i class="ion-ios-remove"></i>
                            </button>
                        </span>
                        <input type="number" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100"placeholder="Quantité" value="{{ old('quantity') }}">
                        <span class="input-group-btn ml-2">
                            <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                                <i class="ion-ios-add"></i>
                            </button>
                        </span>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="product" value="{{ $product->id }}" />
                    </div>
                     {{-- <button type="submit" class="btn btn-primary py-3 px-5"> Add to cart </button> --}}
                </div>
                <p>
                    <button type="submit" id="btn_product" class="btn btn-primary col-sm-12 py-3 px-5 btn-product" ><i class="fa fa-cart-plus"></i> Ajouter au panier</button>
                </p>
            </div>
        </div>
    </div>
</section>
</form>

{{-- 
<div class="container products">

<div class="row">
    <div class="col-md-6">
        <ul id="thumbnails" class="col-md-4 list-unstyled">
            <li>
                <a href="javascript: void(0)">
                    @if(isset($product->cover))
                    <img class="img-responsive img-thumbnail"
                         src="{{ asset("storage/$product->cover") }}"
                         alt="{{ $product->name }}" />
                    @else
                    <img class="img-responsive img-thumbnail"
                         src="{{ asset("https://placehold.it/180x180") }}"
                         alt="{{ $product->name }}" />
                    @endif
                </a>
            </li>
            @if(isset($images) && !$images->isEmpty())
                @foreach($images as $image)
                <li>
                    <a href="javascript: void(0)">
                    <img class="img-responsive img-thumbnail"
                         src="{{ asset("storage/$image->src") }}"
                         alt="{{ $product->name }}" />
                    </a>
                </li>
                @endforeach
            @endif
        </ul>
        <figure class="text-center product-cover-wrap col-md-8">
            @if(isset($product->cover))
                <img id="main-image" class="product-cover img-responsive"
                     src="{{ asset("storage/$product->cover") }}?w=400"
                     data-zoom="{{ asset("storage/$product->cover") }}?w=1200">
            @else
                <img id="main-image" class="product-cover" src="https://placehold.it/300x300"
                     data-zoom="{{ asset("storage/$product->cover") }}?w=1200" alt="{{ $product->name }}">
            @endif
        </figure>
    </div>
    <div class="col-md-6">
        <div class="product-description">
            <h1>{{ $product->name }}
                <small>{{ config('cart.currency') }} {{ $product->price }}</small>
            </h1>
            <div class="description">{!! $product->description !!}</div>
            <div class="excerpt">
                <hr>{!!  str_limit($product->description, 100, ' ...') !!}</div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    @include('layouts.errors-and-messages')
                    <form action="{{ route('cart.store') }}" class="form-inline" method="post">
                        {{ csrf_field() }}
                        @if(isset($productAttributes) && !$productAttributes->isEmpty())
                            <div class="form-group">
                                <label for="productAttribute">Choose Combination</label> <br />
                                <select name="productAttribute" id="productAttribute" class="form-control select2">
                                    @foreach($productAttributes as $productAttribute)
                                        <option value="{{ $productAttribute->id }}">
                                            @foreach($productAttribute->attributesValues as $value)
                                                {{ $value->attribute->name }} : {{ ucwords($value->value) }}
                                            @endforeach
                                            @if(!is_null($productAttribute->price))
                                                ( {{ config('cart.currency_symbol') }} {{ $productAttribute->price }})
                                            @endif
                                        </option>
                                    @endforeach
                                </select>
                            </div><hr>
                        @endif
                        <div class="form-group">
                            <input type="text"
                                   class="form-control"
                                   name="quantity"
                                   id="quantity"
                                   placeholder="Quantity"
                                   value="{{ old('quantity') }}" />
                            <input type="hidden" name="product" value="{{ $product->id }}" />
                        </div>
                        <button type="submit" class="btn btn-warning"><i class="fa fa-cart-plus"></i> Add to cart
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div> --}}

@section('js')

    <script src="{{ asset('js/front.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script> 
    <script type="text/javascript">
        $(document).ready(function () {
            var productPane = document.querySelector('.product-cover');
            var paneContainer = document.querySelector('.product-cover-wrap');

            new Drift(productPane, {
                paneContainer: paneContainer,
                inlinePane: false
            });
        });
        $(document).ready(function(){

            var quantitiy=0;
            $('.quantity-right-plus').click(function(e){
                    
                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity = parseInt($('#quantity').val());
                    
                    // If is not undefined
                        
                        $('#quantity').val(quantity + 1);

                    
                        // Increment
                    
                });

                $('.quantity-left-minus').click(function(e){
                    // Stop acting like a button
                    e.preventDefault();
                    // Get the field name
                    var quantity = parseInt($('#quantity').val());
                    
                    // If is not undefined
                
                        // Increment
                        if(quantity>0){
                        $('#quantity').val(quantity - 1);
                        }
                });
                
            });
    </script>
@endsection