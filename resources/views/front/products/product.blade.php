@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="product"/>
    <meta property="og:title" content="{{ $product->name }}"/>
    <meta property="og:description" content="{{ strip_tags($product->description) }}"/>
    @if(!is_null($product->cover))
        <meta property="og:image" content="{{ asset("storage/$product->cover") }}"/>
    @endif
@endsection

@section('content')
    <div class="hero-wrap hero-bread" style="background-image: url('{{asset("front/images/united.png")}}');">
        <div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-0 bread">{{ucfirst($product->name)}}</h1>
                    {{-- <hr> --}}
                    <p class="breadcrumbs">
                        <span class="mr-2"><a href="{{ route('home') }}"> <i class="fa fa-home"></i>Accueil</a> /</span>
                        @if(isset($category))
                        <span class="mr-2"><a href="{{ route('front.category.slug', $category->slug) }}">{{$category->name}}</a> /</span> 
                        @endif
                        <span class="active">Produit</span>
                    </p>
                </div>
            </div>
        </div>
    </div>
    @include('layouts.front.product')
@endsection