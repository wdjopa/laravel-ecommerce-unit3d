@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="category"/>
    <meta property="og:title" content="{{ $category->name }}"/>
    <meta property="og:description" content="{{ $category->description }}"/>
    @if(!is_null($category->cover))
        <meta property="og:image" content="{{ asset("storage/$category->cover") }}"/>
    @endif
@endsection

@section('content')
<div class="hero-wrap hero-bread" style="background-color: transparent;">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">Collection</h1>
        {{-- <h3> ~ {{$category->name}} ~</h3> --}}
        <p class="breadcrumbs"><span class="mr-2"><a href="{{route('home')}}">Accueil</a></span> / <span>Collection {{$category->name}}</span></p>
        </div>
      </div>
    </div>
  </div>

    <section class="ftco-section bg-light">
      <div class="container-fluid">

            @if(sizeof($products) == 0)
            <h2 style="text-align: center">
                Aucun produit n'est disponible dans cette collection
            </h2>
            @endif
            <div class="row">

            @include('front.products.product-list', ['products' => $products])

              {{-- <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-1.jpg" alt="Colorlib Template">
                          <span class="status">30%</span>
                      </a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span class="mr-2 price-dc">$120.00</span><span class="price-sale">$80.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-2.jpg" alt="Colorlib Template">
                          <span class="status">New Arrival</span>
                      </a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span>$120.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-3.jpg" alt="Colorlib Template"></a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span>$120.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-4.jpg" alt="Colorlib Template"></a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span>$120.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-5.jpg" alt="Colorlib Template"></a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span>$120.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-6.jpg" alt="Colorlib Template"></a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span>$120.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-7.jpg" alt="Colorlib Template"></a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span>$120.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div>
              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                  <div class="product">
                      <a href="#" class="img-prod"><img class="img-fluid" src="images/product-8.jpg" alt="Colorlib Template">
                          <span class="status">Best Sellers</span>
                      </a>
                      <div class="text py-3 px-3">
                          <h3><a href="#">Young Woman Wearing Dress</a></h3>
                          <div class="d-flex">
                              <div class="pricing">
                                  <p class="price"><span>$120.00</span></p>
                              </div>
                              <div class="rating">
                                  <p class="text-right">
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                      <span class="ion-ios-star-outline"></span>
                                  </p>
                              </div>
                          </div>
                          <hr>
                          <p class="bottom-area d-flex">
                              <a href="#" class="add-to-cart"><span>Add to cart <i class="ion-ios-add ml-1"></i></span></a>
                              <a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
                          </p>
                      </div>
                  </div>
              </div> --}}
          </div>
        {{-- <div class="row mt-5">
            <div class="col text-center">
                <div class="block-27">
                    <ul>
                        <li><a href="#">&lt;</a></li>
                        <li class="active"><span>1</span></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&gt;</a></li>
                    </ul>
                </div>
            </div>
        </div> --}}
      </div>
  </section>
 
{{-- <div class="container">
        <hr>
        <div class="row">
            <div class="category-top col-md-12">
                <h2>{{ $category->name }}</h2>
                {!! $category->description !!}
            </div>
        </div>
        <hr>
        <div class="col-md-3">
            @include('front.categories.sidebar-category')
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="category-image">
                    @if(isset($category->cover))
                        <img src="{{ asset("storage/$category->cover") }}" alt="{{ $category->name }}" class="img-responsive" />
                    @else
                        <img src="https://placehold.it/1200x200" alt="{{ $category->cover }}" class="img-responsive" />
                    @endif
                </div>
            </div>
            <hr>
            <div class="row">
                @include('front.products.product-list', ['products' => $products])
            </div>
        </div>
    </div> --}}
@endsection
