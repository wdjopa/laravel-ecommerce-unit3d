<ul class="navbar-nav ml-auto">
    <li class="nav-item active"><a href="{{route("home")}}" class="nav-link">Accueil</a></li>
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="{{route("home")}}" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Collections</a>
        <div class="dropdown-menu" aria-labelledby="dropdown04">
            <?php
                $collections = App\Shop\Categories\Category::all();
            ?>
            @foreach($collections as $collection)
                <a class="dropdown-item" href="{{route("front.category.slug", $collection->slug)}}">{{ucfirst($collection->name)}}</a>
            @endforeach
        </div>
    </li>
    <li class="nav-item"><a href="about.html" class="nav-link">About</a></li>
    <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>
    <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li>
    <li class="nav-item cta cta-colored"><a href="{{route("cart.index")}}" class="nav-link"><span class="icon-shopping_cart"></span>({{ $cartCount }})</a></li>

</ul>