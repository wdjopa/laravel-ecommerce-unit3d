@extends('layouts.front.app')

@section('og')
    <meta property="og:type" content="home"/>
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
@endsection

@section('content')
    @include('layouts.front.home-slider')
    <div class="goto-here"></div>

    <section class="ftco-section ftco-product">
    	<div class="container">
    		<div class="row justify-content-center mb-3 pb-3">
          <div class="col-md-12 heading-section text-center ftco-animate">
          	<h1 class="big">Collections</h1>
            <h2 class="mb-4">Collections</h2>
          </div>
        </div>
    		<div class="row">
    			<div class="col-md-12">
    				<div class="product-slider owl-carousel ftco-animate">
                        @foreach($categories as $category)
    					<div class="item">
		    				<div class="product">
                            <a href="{{ route ("front.category.slug", $category->slug)}}" class="img-prod">
                                    <img class="img-fluid" src="{{ asset("storage/$category->cover")}}">
		    						{{-- <span class="status">30%</span> --}}
		    					</a>
		    					<div class="text text-center pt-3 px-3">
		    						<h3><a href="{{ route ("front.category.slug", $category->slug)}}">{{ ucfirst($category->name) }}</a></h3>
                                    {{-- 
		    						<div class="d-flex">
                                            <div class="pricing">
                                                <p class="price"><span class="mr-2 price-dc">$120.00</span><span class="price-sale">$80.00</span></p>
                                            </div>
                                            <div class="rating">
                                                <p class="text-right">
                                                    <span class="ion-ios-star-outline"></span>
                                                    <span class="ion-ios-star-outline"></span>
                                                    <span class="ion-ios-star-outline"></span>
                                                    <span class="ion-ios-star-outline"></span>
                                                    <span class="ion-ios-star-outline"></span>
                                                </p>
                                            </div> 
                                        </div>
                                        --}}
		    					</div>
		    				</div>
                        </div>
                        @endforeach
    				</div>
    			</div>
    		</div>
    	</div>
    </section>

    
    <section class="ftco-section ftco-no-pb ftco-no-pt bg-light">
			<div class="container">
				<div class="row">
					<div class="col-md-5 p-md-5 img img-2 d-flex justify-content-center align-items-center" style="background-image: url({{asset('front/images/5.png')}});">
						<a href="https://www.youtube.com/watch?v=1Zfv1LZlU9U" class="icon popup-vimeo d-flex justify-content-center align-items-center">
							<span class="icon-play"></span>
						</a>
					</div>
					<div class="col-md-7 py-5 wrap-about pb-md-5 ftco-animate">
	          <div class="heading-section-bold mb-5 mt-md-5">
	          	<div class="ml-md-0">
		            <h2 class="mb-4">The Unit 3D <br> <span>Fashion Shop</span></h2>
	            </div>
	          </div>
	          <div class="pb-md-5">
							<p>
                                UNIT3D is born on November 15, 2015,  by Founder, Kevin Alima. In 2017, he partnered with longtime best friend to continue building a brand For The People.
                            </p>
                            <p>
                                We represent a new and innovative movement in the order of fashion - with a focus on maintaining its products affordable while staying stylish and comfortable.
                            </p>
                            <p>
                                We are the lovechild of fashion, design, art, culture, and sharing the lifestyle. We make wearable threads that don’t hurt your pocket. We make you good-looking with the minimum spent. 
                            </p>
                            <p>
                                We aim to spread messages and start conversations. We strive to be more than a brand - we are For The People. We work a lot with home-made labour and ensure they work under respectful conditions. All of our fits and styles are timeless, classic and affordable. 
                            </p>
                            <p>
                            Fashion is one of the most potent vessels for spreading a movement, sparking ideas and conversations. UNIT3D represents a way of engaging with the community, raising awareness and helping the community voice its thoughts uniquely. 
                            <p>
                                <b>
                                    For The People
                                </b>
                            </p>
                            </p>
                            <p>    
                            The UNIT3D Team</p>
						</div>
					</div>
				</div>
			</div>
		</section>
    {{-- @if($cat1->products->isNotEmpty())      
    	<section class="ftco-section ftco-product">
            <div class="container">
                <div class="row justify-content-center mb-3 pb-3">
                    <div class="col-md-12 heading-section text-center ftco-animate">
                        <h1 class="big">{{ $cat1->name }}</h1>
                        <h2 class="mb-4">{{ $cat1->name }}</h2>
                    </div>
                </div>
                <div class="row">
                    @include('front.products.product-list', ['products' => $cat1->products->where('status', 1)])
                </div>
            </div>
        </section>
    @endif
    @if($cat2->products->isNotEmpty())
        <section class="ftco-section ftco-product bg-light">
            <div class="container">
                <div class="row justify-content-center mb-3 pb-3">
                    <div class="col-md-12 heading-section text-center ftco-animate">
                        <h1 class="big">{{ $cat2->name }}</h1>
                        <h2 class="mb-4">{{ $cat2->name }}</h2>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                    <div class="row">
                    @include('front.products.product-list', ['products' => $cat2->products->where('status', 1)])
                </div>
            </div>
        </section>
    @endif --}}
    {{-- <hr /> --}}
    {{-- @include('mailchimp::mailchimp') --}}
    
    
    <section class="ftco-section bg-light">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <h1 class="big">Produits</h1>
                    <h2 class="mb-4">Nos Produits</h2>
                </div>
            </div>    		
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-slider owl-carousel ftco-animate">
                        @foreach($products as $product)
                            <div class="product">
                                <a href="{{route("front.get.product", $product->slug)}}" class="img-prod">
                                    {{-- {{$product}} --}}
                                    <img class="img-fluid" src="{{ asset("storage/$product->cover")}}" alt="{{$product->name}}">
                                </a>
                                <div class="text py-3 px-3">
                                    <h3><a href="{{route("front.get.product", $product->slug)}}">{{$product->name}}</a></h3>
                                    <div class="d-flex">
                                        <div class="pricing">
                                            <p class="price"><span>${{$product->price}}</span></p>
                                        </div>
                                        <div class="rating">
                                            <p class="text-right">
                                                <span class="ion-ios-star-outline"></span>
                                                <span class="ion-ios-star-outline"></span>
                                                <span class="ion-ios-star-outline"></span>
                                                <span class="ion-ios-star-outline"></span>
                                                <span class="ion-ios-star-outline"></span>
                                            </p>
                                        </div>
                                    </div>
                                    <hr>
                                    <p class="bottom-area d-flex">
                                        <a href="#" class="add-to-cart"><span>Ajouter au panier <i class="ion-ios-add ml-1"></i></span></a>
                                        <a href="{{route("front.get.product", $product->slug)}}" class="ml-auto"><span><!-- <i class="ion-ios-heart-empty"></i>--> Détails</span></a>
                                    </p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section class="ftco-section ftco-section-more img" style="background-image: url({{asset("front/images/promo2.png")}});">
        
        <div class="container">
    		<div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section ftco-animate">
                    <h2>Promotions</h2>
                </div>
            </div>
    	</div>
    </section>

    
    <section class="ftco-section testimony-section bg-light">
        <div class="container">
            <div class="row justify-content-center mb-3 pb-3">
                <div class="col-md-12 heading-section text-center ftco-animate">
                    <h1 class="big">Témoignages</h1>
                </div>
            </div>    		
            <div class="row justify-content-center">
                <div class="col-md-8 ftco-animate">
                    <div class="row ftco-animate">
                        <div class="col-md-12">
                            <div class="carousel-testimony owl-carousel ftco-owl">
                                <div class="item">
                                    <div class="testimony-wrap py-4 pb-5">
                                        <div class="user-img mb-4" style="background-image: url(https://djopa.fr/admin/images/profile.jpg)">
                                            <span class="quote d-flex align-items-center justify-content-center">
                                            <i class="icon-quote-left"></i>
                                            </span>
                                        </div>
                                        <div class="text text-center">
                                            <p class="mb-4">Ici, il y'aura les messages des clients en fonction de ce qu'ils pensent. Vos vêtements sont très beau et en plus très abordable niveau prix. Je ferai sûrement des achats</p>
                                            <p class="name">Wilfried Djopa</p>
                                            <span class="position">Client</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    <section class="ftco-section ftco-counter img" id="section-counter" style="background-image: url({{asset("front/images/bg_4.jpg")}});">
    	<div class="container">
    		<div class="row justify-content-center py-5">
    			<div class="col-md-10">
		    		<div class="row">
		          <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="452">0</strong>
		                <span>Clients satisfaits</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="3">0</strong>
		                <span>Villes</span>
		              </div>
		            </div>
		          </div>
		          <div class="col-md-4 d-flex justify-content-center counter-wrap ftco-animate">
		            <div class="block-18 text-center">
		              <div class="text">
		                <strong class="number" data-number="32">0</strong>
		                <span>Produits</span>
		              </div>
		            </div>
		          </div>
		        </div>
	        </div>
        </div>
    	</div>
    </section>

    <section class="ftco-section bg-light ftco-services">
            <div class="container">
                <div class="row justify-content-center mb-3 pb-3">
              <div class="col-md-12 heading-section text-center ftco-animate">
                <h1 class="big">Services</h1>
                <h2>Un service de qualité pour vous</h2>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services">
                  <div class="icon d-flex justify-content-center align-items-center mb-4">
                        <span class="flaticon-002-recommended"></span>
                  </div>
                  <div class="media-body">
                    <h3 class="heading">Service Premium</h3>
                    <p>Contactez notre équipe disponible 24h/24 et 7j/7.</p>
                  </div>
                </div>      
              </div>
              <div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services">
                  <div class="icon d-flex justify-content-center align-items-center mb-4">
                        <span class="flaticon-001-box"></span>
                  </div>
                  <div class="media-body">
                    <h3 class="heading">Livraison à domicile</h3>
                    <p>Nous vous livrons à domicile partout dans les villes de Yaoundé, Douala & Bangangté</p>
                  </div>
                </div>    
              </div>
              <div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services">
                  <div class="icon d-flex justify-content-center align-items-center mb-4">
                        <span class="flaticon-003-medal"></span>
                  </div>
                  <div class="media-body">
                    <h3 class="heading">Qualité supérieure</h3>
                    <p>Box tee, hoodies, sacs & chapeaux de qualité supérieure.</p>
                  </div>
                </div>      
              </div>
            </div>
            </div>
        </section>
              

@endsection