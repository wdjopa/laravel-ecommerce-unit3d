@extends('layouts.front.app')

@section('content')
        {{-- <div class="container product-in-cart-list">
            @if(!$cartItems->isEmpty())
                <div class="row">
                    <div class="col-md-12">
                        <ol class="breadcrumb">
                            <li><a href="{{ route('home') }}"> <i class="fa fa-home"></i> Home</a></li>
                            <li class="active">Cart</li>
                        </ol>
                    </div>
                    <div class="col-md-12 content">
                        <div class="box-body">
                            @include('layouts.errors-and-messages')
                        </div>
                        <h3><i class="fa fa-cart-plus"></i> Shopping Cart</h3>
                        <table class="table table-striped">
                            <thead>
                                <th class="col-md-2 col-lg-2">Cover</th>
                                <th class="col-md-2 col-lg-5">Name</th>
                                <th class="col-md-2 col-lg-2">Quantity</th>
                                <th class="col-md-2 col-lg-1"></th>
                                <th class="col-md-2 col-lg-2">Price</th>
                            </thead>
                            <tfoot>
                            <tr>
                                <td class="bg-warning">Subtotal</td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning">{{config('cart.currency')}} {{ number_format($subtotal, 2, '.', ',') }}</td>
                            </tr>
                            @if(isset($shippingFee) && $shippingFee != 0)
                            <tr>
                                <td class="bg-warning">Shipping</td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning">{{config('cart.currency')}} {{ $shippingFee }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td class="bg-warning">Tax</td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning"></td>
                                <td class="bg-warning">{{config('cart.currency')}} {{ number_format($tax, 2) }}</td>
                            </tr>
                            <tr>
                                <td class="bg-success">Total</td>
                                <td class="bg-success"></td>
                                <td class="bg-success"></td>
                                <td class="bg-success"></td>
                                <td class="bg-success">{{config('cart.currency')}} {{ number_format($total, 2, '.', ',') }}</td>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($cartItems as $cartItem)
                                <tr>
                                    <td>
                                        <a href="{{ route('front.get.product', [$cartItem->product->slug]) }}" class="hover-border">
                                            @if(isset($cartItem->cover))
                                                <img src="{{$cartItem->cover}}" alt="{{ $cartItem->name }}" class="img-responsive img-thumbnail">
                                            @else
                                                <img src="https://placehold.it/120x120" alt="" class="img-responsive img-thumbnail">
                                            @endif
                                        </a>
                                    </td>
                                    <td>
                                        <h3>{{ $cartItem->name }}</h3>
                                        @if($cartItem->options->has('combination'))
                                            @foreach($cartItem->options->combination as $option)
                                                <small class="label label-primary">{{$option['value']}}</small>
                                            @endforeach
                                        @endif
                                        <div class="product-description">
                                            {!! $cartItem->product->description !!}
                                        </div>
                                    </td>
                                    <td>
                                        <form action="{{ route('cart.update', $cartItem->rowId) }}" class="form-inline" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="put">
                                            <div class="input-group">
                                                <input type="text" name="quantity" value="{{ $cartItem->qty }}" class="form-control" />
                                                <span class="input-group-btn"><button class="btn btn-default">Update</button></span>
                                            </div>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            <button onclick="return confirm('Are you sure?')" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                        </form>
                                    </td>
                                    <td>{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="btn-group pull-right">
                                    <a href="{{ route('home') }}" class="btn btn-default">Continue shopping</a>
                                    <a href="{{ route('checkout.index') }}" class="btn btn-primary">Go to checkout</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-12">
                        <p class="alert alert-warning">No products in cart yet. <a href="{{ route('home') }}">Shop now!</a></p>
                    </div>
                </div>
            @endif
        </div> --}}
        
        <div class="hero-wrap hero-bread" style="background-image: url('images/bg_6.jpg');">
		<div class="container">
            <div class="row no-gutters slider-text align-items-center justify-content-center">
                <div class="col-md-9 ftco-animate text-center">
                    <h1 class="mb-0 bread">PANIER</h1>
                <p class="breadcrumbs"><span class="mr-2"><a href="{{route("home")}}">ACCUEIL</a></span> <span>PANIER</span></p>
            </div>
        </div>
    </div>
</div>
@if(!$cartItems->isEmpty())
	<section class="ftco-section ftco-cart">
		<div class="container">
			<div class="row">
				<div class="col-md-12 ftco-animate">
					<div class="cart-list">
						<table class="table">
							<thead class="thead-primary">
								<tr class="text-center">
									<th>&nbsp;</th>
									<th>Image</th>
									<th>Produit</th>
									<th>Prix</th>
									<th>Quantité</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
                                @foreach($cartItems as $cartItem)

								<tr class="text-center">
									<td class="product-remove">
                                        <form action="{{ route('cart.destroy', $cartItem->rowId) }}" method="post" name="deleteCart{{$cartItem->rowId}}">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="delete">
                                            {{-- <input type="submit" onclick="return confirm('Êtes-vous sûr de vouloir retirer ce produit du panier ?')"><span class="ion-ios-close"></span></button>--}}
                                            <a href="#!" onclick="if(confirm('Êtes-vous sûr de vouloir retirer ce produit du panier ?')) document.deleteCart{{$cartItem->rowId}}.submit()"><span class="ion-ios-close"></span></a> 
                                        </form>
                                    </td>

									<td class="image-prod">
                                        <a href="{{ route('front.get.product', [$cartItem->product->slug]) }}" class="hover-border">
                                            <div class="img" style="background-image:url('{{$cartItem->cover}}');">
                                            </div>
                                        </a>
									</td>

									<td class="product-name">
										<h3>{{$cartItem->name}}</h3>
										<p> 
                                             @if($cartItem->options->has('combination'))
                                                <b>Modèle :</b>
                                                @foreach($cartItem->options->combination as $option)
                                                <small class="label label-primary" style="text-transform: uppercase">{{$option['value']}},</small>
                                                @endforeach
                                            @endif
                                        </p>
									</td>

									<td class="price">{{config('cart.currency')}} {{ number_format($cartItem->price, 2) }}</td>

									<td class="quantity">
                                        <form action="{{ route('cart.update', $cartItem->rowId) }}" class="form-inline" name="updateCart{{$cartItem->rowId}}" method="post">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="put">
                                            <input type="hidden" name="quantity" required class="form-control" />
                                            {{-- <div class="input-group">
                                                <span class="input-group-btn"><button class="btn btn-default">Update</button></span>
                                            </div>
                                             --}}
                                        </form>
                                            <div class="input-group mb-3">
                                                <input type="number" name="quantity" class="quantity form-control input-number" required value="{{ $cartItem->qty }}" onchange="document.updateCart{{$cartItem->rowId}}.quantity.value=this.value; if(document.updateCart{{$cartItem->rowId}}.quantity.value>0)document.updateCart{{$cartItem->rowId}}.submit()" min="1" max="100">
                                            </div>
									</td>

									<td class="total">{{config('cart.currency')}} {{ number_format(($cartItem->price*$cartItem->qty), 2) }}</td>
								</tr><!-- END TR-->
                                @endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="row">                
                <div class="col col-lg-7 col-sm-12 col-md-12 mt-5 cart-wrap ftco-animate">
                    <div class="cart-total">
                        {{-- <label for="">Message</label> --}}
                        <textarea name="message" id="" placeholder="Message à la commande (facultatif)" style="border: 1px dashed #DDDDDDDD; width: 100%; resize: none; padding: 0.5rem; box-sizing: border-box;" cols="30" rows="10"></textarea>
                    </div>
                </div>
				<div class="col col-lg-5 col-sm-12 col-md-12 mt-5 cart-wrap ftco-animate">
                    <div class="cart-total mb-3">
						<h3>Totaux</h3>
						<p class="d-flex">
							<span>Sous-total</span>
							<span>{{config('cart.currency')}} {{ number_format($subtotal, 2, '.', ',') }}</span>
						</p>
						<p class="d-flex">
							<span>Livraison</span>
							<span>{{config('cart.currency')}} {{ number_format($shippingFee, 2, '.', ',') }}</span>
						</p>
						{{-- <p class="d-flex">
							<span>Discount</span>
							<span>{{config('cart.currency')}} {{ number_format($discount=20, 2, '.', ',') }}</span>
						</p> --}}
						<hr>
						<p class="d-flex total-price">
							<span>Total</span>
							<span>{{config('cart.currency')}} {{ number_format($total, 2, '.', ',') }}</span>
						</p>
					</div>
                <p class="text-center"><a href="{{route("checkout.index")}}" class="btn btn-primary py-3 px-4">Procéder au paiement</a></p>
				</div>
			</div>
		</div>
	</section>
@else
    <section class="ftco-section bg-light">
        <h2 style="text-align: center;font-weight: 300;">
            Votre panier est vide.
        </h2>
    </section>
@endif
	<section class="ftco-section bg-light">
		<div class="container">
			<div class="row justify-content-center mb-3 pb-3">
				<div class="col-md-12 heading-section text-center ftco-animate">
					<h1 class="big">Produits</h1>
					<h2 class="mb-4">Peut vous intéresser</h2>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<div class="row">
				<div class="col-sm col-md-6 col-lg ftco-animate">
					<div class="product">
						<a href="#" class="img-prod"><img class="img-fluid" src="images/product-1.jpg"
								alt="Colorlib Template"></a>
						<div class="text py-3 px-3">
							<h3><a href="#">Young Woman Wearing Dress</a></h3>
							<div class="d-flex">
								<div class="pricing">
									<p class="price"><span>$120.00</span></p>
								</div>
								<div class="rating">
									<p class="text-right">
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
									</p>
								</div>
							</div>
							<hr>
							<p class="bottom-area d-flex">
								<a href="#" class="add-to-cart"><span>Add to cart <i
											class="ion-ios-add ml-1"></i></span></a>
								<a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm col-md-6 col-lg ftco-animate">
					<div class="product">
						<a href="#" class="img-prod"><img class="img-fluid" src="images/product-2.jpg"
								alt="Colorlib Template"></a>
						<div class="text py-3 px-3">
							<h3><a href="#">Young Woman Wearing Dress</a></h3>
							<div class="d-flex">
								<div class="pricing">
									<p class="price"><span>$120.00</span></p>
								</div>
								<div class="rating">
									<p class="text-right">
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
									</p>
								</div>
							</div>
							<hr>
							<p class="bottom-area d-flex">
								<a href="#" class="add-to-cart"><span>Add to cart <i
											class="ion-ios-add ml-1"></i></span></a>
								<a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm col-md-6 col-lg ftco-animate">
					<div class="product">
						<a href="#" class="img-prod"><img class="img-fluid" src="images/product-3.jpg"
								alt="Colorlib Template"></a>
						<div class="text py-3 px-3">
							<h3><a href="#">Young Woman Wearing Dress</a></h3>
							<div class="d-flex">
								<div class="pricing">
									<p class="price"><span>$120.00</span></p>
								</div>
								<div class="rating">
									<p class="text-right">
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
									</p>
								</div>
							</div>
							<hr>
							<p class="bottom-area d-flex">
								<a href="#" class="add-to-cart"><span>Add to cart <i
											class="ion-ios-add ml-1"></i></span></a>
								<a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
							</p>
						</div>
					</div>
				</div>
				<div class="col-sm col-md-6 col-lg ftco-animate">
					<div class="product">
						<a href="#" class="img-prod"><img class="img-fluid" src="images/product-4.jpg"
								alt="Colorlib Template"></a>
						<div class="text py-3 px-3">
							<h3><a href="#">Young Woman Wearing Dress</a></h3>
							<div class="d-flex">
								<div class="pricing">
									<p class="price"><span>$120.00</span></p>
								</div>
								<div class="rating">
									<p class="text-right">
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
										<span class="ion-ios-star-outline"></span>
									</p>
								</div>
							</div>
							<hr>
							<p class="bottom-area d-flex">
								<a href="#" class="add-to-cart"><span>Add to cart <i
											class="ion-ios-add ml-1"></i></span></a>
								<a href="#" class="ml-auto"><span><i class="ion-ios-heart-empty"></i></span></a>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    
@endsection
@section('css')
    <style type="text/css">
        .product-description {
            padding: 10px 0;
        }
        .product-description p {
            line-height: 18px;
            font-size: 14px;
        }
    </style>
@endsection