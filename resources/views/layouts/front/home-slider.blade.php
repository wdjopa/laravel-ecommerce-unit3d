<section id="hero" class="hero-section top-area">
    {{-- <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="hero-content">
                    <h1 class="hero-title">{{config('app.name')}} Holiday <br> Collection</h1>
                    <p class="hero-text">Download this amazing e-commerce web app for <strong class="text-success">FREE!</strong></p>
                    <a class="btn btn-success" href="https://github.com/jsdecena/laracom" target="_blank" role="button">DOWNLOAD <i class="fa fa-cloud-download"></i></a>
                    <a class="btn btn-danger" href="https://m.do.co/c/bce94237de96" target="_blank" role="button">Build on DigitalOcean <i class="fa fa-paper-plane"></i></a>
                </div>
            </div>
            <div class="col-md-4"></div>
        </div>
    </div> --}}
    
	<div class="hero-wrap js-fullheight" style="background-image: url('{{asset('front/images/promo.png')}}');">
		<div class="container">
			<div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
				<h3 class="v">The Unit 3D Shop</h3>
				<h3 class="vr">Since 2015</h3>
				<div class="col-md-11 ftco-animate text-center">
					<h1>UNIT 3D</h1>
					<h2><span>For The People</span></h2>
				</div>
				<div class="mouse">
					<a href="#" class="mouse-icon">
						<div class="mouse-wheel"><span class="ion-ios-arrow-down"></span></div>
					</a>
				</div>
			</div>
        </div>
		<div class="overlay"></div>
        
	</div>
</section>

